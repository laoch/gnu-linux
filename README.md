# GNU/Linux Series

Date: Sept 2021

GNU/Linux Series material

+ odp
  - Linux-Part_1_Notes.pdf - Slides from day 1
  - Linux-Part_2_Notes.pdf - Slides from day 2
  - Linux-Part_3_Notes.pdf - Slides from day 3
  - Linux-Part_4_Notes.pdf - Slides from day 4
  - WS1_3-LAMP_Server_Notes_odp.pdf
  - WS2_3-MariaDB_SQL_Notes_odp.pdf
  - WS3_3-MariaDB_Replication_Notes_odp.pdf
+ odt
  - System_Administration_v2.0.pdf - Overall System Administration document
  - WS1_3-LAMP_Server_odt.pdf
  - WS2_3-MariaDB_SQL_odt.pdf
  - WS3_3-MariaDB_Replication_odt.pdf
  - SQLite_odt.pdf
  - Build-Linode-Cloud-Based-Testbed.pdf
+ Labs
  - Linux-Laboratory_2.pdf - Laboratory document - Day 2
  - Linux-Laboratory_3.pdf - Laboratory document - Day 3
  - Linux-Laboratory_4.pdf - Laboratory document - Day 4




